import React from 'react';
import { StyleSheet, Text } from 'react-native';

export class Logo extends React.Component {
  render() {
    return <Text style={styles.logo}>space</Text>;
  }
}

const styles = StyleSheet.create({
  logo: {
    position: 'absolute',
    width: 48,
    height: 23,
    left: 24,
    top: 24,
    fontSize: 20,
    fontFamily: 'space-font',
    color: '#FFFFFF',
  },
});
