import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { AppLoading, Asset, Font, Icon } from 'expo';
import { Logo } from './components/Logo.js';

export default class App extends React.Component {
  state = {
    fontLoaded: false,
  };

  componentDidMount() {
    this.loadAssetsAsync()
  }
  loadAssetsAsync = async () => {
    await Font.loadAsync({
      'space-font': require('./assets/fonts/TTCommons-Regular.ttf'),
    })
    this.setState({ fontLoaded: true })
  }

  render() {

    if (!this.state.fontLoaded) {
      return <AppLoading />
    }

    return (
      <View style={styles.container}>
        <Logo />
        <Text style={styles.text}>Open mystery world around yourself</Text>
        <Text style={styles.login}>Sign in with Google or if have an account Sign up</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000',
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'space-font',
  },
  text: {
    color: '#FFFFFF',
    fontSize: 24,
    fontFamily: 'space-font',
    textAlign: 'center',
  },
  login: {
    position: 'absolute',
    width: 210,
    bottom: 25,
    fontFamily: 'space-font',
    fontSize: 16,
    textAlign: 'center',
    color: 'rgba(255, 255, 255, 0.3)',
  },
});
